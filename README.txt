I. Introduction

Git for Gat is a programming game using a git repository: The object 
of each turn is to modify this program in a way that will change the 
output without introducing a bug. In some ways, it is like the folklore 
game "telephone" (https://en.wikipedia.org/wiki/Chinese_whispers). To see
what the current program is doing, compare the files in the directory
input_files with those in output_files.

You should make at least 2 changes to the system: add one more sample
input .txt file (as per A) and add some additional functions to the
program . Your coding change should be regular and meaningful. The
sample program inserts instances of "uh", "well" and similar words (so
called hedge phrases) into text to make any text seem more
conversational.  There are several options that you have for making
changes, but these will be discussed later on, as well as a few
constraints about what not to do. Another challenge will be to make
sure your changes are compatible with nearly-simultaneous changes made
by your classmates.

II.  To test the current version of the program, do the following:
      1) download the most current version from github
      2) from the git_for_gat directory load the program in the IDE of your choice.
      3) execute the command:  git_game()
      4) compare the files in input_files with the files in
          output_files to get a clear idea of what the program is
          doing so far.

III. What you are expected to do as your "turn" in the game
      1) Add one more .txt file to the input_directory.  This will
           improve the breadth of the program as it will have to
           handle more diverse types of text. Please choose public
           domain text or other text that is not subject to intellectual
	   property constraints on it usage.  See how the code runs on
	   your new file and try to correct any problems that this text may
	   have caused. I downloaded two of the .txt files in that
	   directory from Project gutenberg (www.gutenberg.org) and
	   changed the filenames to be more explanatory. The remaining
	   file (gnu_manifesto.txt) was downloaded as a text file and then
	   converted to text using the linux utility program "html2text".
	   I then deleted some garbage text at the beginning and end of
	   the file.  I suggest similar methods for finding a file.
      2) Make one or more changes in the code.
           See below (section V) for some examples of types of changes.
      3) Update this Readme by adding a short description of your
           change to the revision notes at the end of this file.  When
           you "push" your changes into the github repository, you can
           use this same text or something similar in the explanation
           of changes.  Subsequent pushes may require further
           modifications in the code and other short explanations.
     4) By the time that you add your changes to github, other
     	  students may have already have done so.  So you must be
	  prepared to make any needed changes to prevent the
	  programming from producing an error. I suggest making
	  a copy of your code before attempting to submit it and
	  then handle the compatibility issues using a subset of
	  the following strategies:
	  i) after you have made all your changes, download a fresh
	      copy of the code, remake your changes, retest
	      and then submit.
	  ii) when you do "pushes" and "pulls" of the files from
	      the repository, you will get messages about
	      incompatibilities. You can resolve these and then
	      resubmit.

IV. The code is organized as follows:
   1) The main directory is called git_for_gat
   2) There are two subdirectories: input_files and output_files
   3) There are 2 program files:
      a) git_game_functions.py -- it is unlikely that you will need to change this file
      b) filter_functions.py -- this is the file that you will most likely be modifying.
  4) It is possible that you will want to add additional files. Please do this very sparingly.
  5) To run one version of the file, simply execute the command "git_game()" after
       loading git_game_functions.py in python.

IV. The following are sample instances of the types of modifications I am anticipating.

   1) Add a filter that is functionally similar to
       "adams_uh_function".  It should modify one sentence in some
       way: changing capitalization, adding words, removing letters,
       changing some letters to other letters, etc.  The one
       limitation is that the end result text should still be
       readable, even if weirder than before.  I suggest using
       split_by_spaces and join_tokens in some way that is imilar to
       what I did.  The filter will be used if you add it to the
       global variable filter_functions. It is OK to change the order
       of the items in that list as well, possibly inserting yours
       before or after other filters.
       
    2) Write filters that remove uninteresting text from the file.
         For example, the filter could remove Gutenberg's front and
	 end text that it adds to all Project Gutenberg documents.

    3) Add options to the program that will make it easier to
         understand and/or debug. For example, you can add an optional
         "trace" argument that prints names of filters and prints
         sentences before and after each filter is run on them.

     4) Add a filter that optionally converts each line of text into a
          code using a substitutions cypher. You would have to create
          encoding and decoding mappings e.g., a dictionaries mapping
          each letter of the alphabet (or each character) to their
          code and the reverse.  The substitution cypher would either
          have to handle all of utf-8 or else it would have to be
          defined on a subset (like the 52 upper and lowercase
          letters) and it would have to be reversible.  You would
          provide the filter for converting the existing code, as well
          as the program for converting the output back.  You may also
          have to figure out how it interacts with other
          collaborator's filters and provide some guidance for future
          changes, e.g., maybe the subsitution cypher would have to
          follow all other changes.

      5) There are several other possibilities as well.  The basic
           idea is to make some change that either makes for a more
           interesting result or makes the system a better program.

V.  Revisions -- enter your revisions here

    1) Reincoded the files from UTF-8-BOM to UTF-8 due to system having trouble
	 with the former.
    2) Added a new input file called "!test.txt"
    3) Added a new function called "masayas_spelling_mistakes" that randomly adds
	letters to replace any character in a list. 
    4) Due to no permissions, this project had to be uploaded as its own repo.
