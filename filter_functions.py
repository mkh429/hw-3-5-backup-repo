filter_functions = ['adams_uh_function', "masayas_spelling_problems" ]

def token_triple(word):
    triple = []
    before = ''
    middle = ''
    after = ''
    position = 0
    for char in word:
        position +=1
        if char in 'abcdefghijklmnopqrstuvwxyz':
            if after != '':
                after += char
            else:
                middle +=char
        elif middle == '':
            before += char
        elif (after == '') and (char == '-') and (position < len(word)):
            middle += char
        else:
            after += char
    return([before,middle,after])

def split_by_spaces (insentence):
    tokens = []
    for word in insentence.split(' '):
        tokens.append(token_triple(word))
    return(tokens)

def join_tokens(tokens):
    output = ''
    index = 0
    for before,middle,after in tokens:
        index += 1
        output = output + before + middle + after
        if index != len(tokens):
            output += ' '
    return(output)

def adams_uh_function(sentence):
    ## Adam Meyers (aml4)
    import random
    tokens = split_by_spaces(sentence)
    choices = ['uh','well','I mean','um']
    tokens2 = []
    for token in tokens:
        if random.randint(1,100) <=15:
            word = random.choice(choices)
            if len(tokens2) == 0:
                inbetween = ['',word.capitalize(),',']
            else:
                last_token = tokens2[-1]
                if last_token[2] == '':
                    last_token[2]=','
            tokens2.append(['',word,','])
        tokens2.append(token)    
    output = join_tokens(tokens2)
    return(output)

def masayas_spelling_problems(sentence):
    import random
    import string
    words = sentence
    letters = []
    output = ""
    for word in words:
        letters.extend(list(word))
    
    position = 0
    alphabet = list(string.ascii_lowercase)
    for letter in letters:
        if random.randint(0,100) <= 2:
            letters[position] = alphabet[random.randint(0,len(alphabet)-1)]
            
        position += 1
            
    output = "".join(letters)
    print(output)
    #output = botched_sentence
    return(output)

