from filter_functions import *

def alter_sentence(sentence):
    output = sentence
    for filter_func in filter_functions:
        filter_string = filter_func + ' (' + '\'\'\''+output+'\'\'\'' +')'
        ## print(filter_string)
        output = eval(filter_string)
    return(output)

def alter_file(input_file,output_file):
    with open(input_file) as instream,open(output_file,'w', encoding="UTF-8") as outstream:
        
        for sentence in instream:
            outstream.write(alter_sentence(sentence))

def git_game():
    import os
    for infile in os.listdir('input_files'):
        if infile.endswith('.txt'):
            alter_file('input_files'+os.sep+infile,'output_files'+os.sep+infile)
            
            
git_game()
        
